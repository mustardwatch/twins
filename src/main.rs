#![allow(dead_code)]
#![allow(unused_variables)]

use std::env;
use std::fs;
use std::path;
use std::fs::File;
use std::io::Read;
use std::hash::Hash;
use std::collections::HashMap;

// Cryptographic functions
extern crate crypto;
use crypto::digest::Digest;

// Command line parsing library
extern crate clap;
use clap::App;
use clap::Arg;



#[derive(Clone, Debug)]
struct FileDesc {
    name: path::PathBuf,
    size: u64,
}

type FID = usize;

trait FileCollection {
    fn add(&mut self, FileDesc) -> FID;
    fn get(&self, FID) -> &FileDesc;
    fn size(&self) -> usize;
}

struct VecFileCollection {
    store: Vec<FileDesc>
}

impl VecFileCollection {
    fn new() -> VecFileCollection {
        VecFileCollection {store: Vec::new()}
    }
}

impl FileCollection for VecFileCollection {
    fn add(&mut self, fd: FileDesc) -> FID {
        self.store.push(fd);
        return self.store.len() - 1; // this is horrendous, it is not even reentrant
    }

    fn get(&self, idx: FID) -> &FileDesc {
        return &self.store[idx]
    }

    fn size(&self) -> usize {
        return self.store.len();
    }
} 

type GroupedFiles<T> = HashMap<T, Vec<FID>>;

trait Summarizer<T : Eq + Hash> {
    fn check_sum (&mut self, &FileDesc) -> T;
}

struct SizeSummary {

}

impl Summarizer<u64> for SizeSummary {
    fn check_sum (&mut self, f : &FileDesc) -> u64 {
        return f.size;
    }
}

fn read_file_contents (path : &std::path::PathBuf, buf: & mut Vec<u8>) -> std::io::Result<usize> {
    let mut f = try!{File::open(path)};
    return f.read_to_end(buf);
}

struct MD5DigestSummary {
    digest: crypto::md5::Md5,
}

impl Summarizer<String> for MD5DigestSummary {
    fn check_sum (&mut self, f : &FileDesc) -> String {
        let mut data : Vec<u8> = Vec::new();
        let _ = read_file_contents(&f.name, &mut data);
        self.digest.reset();
        self.digest.input(data.as_slice()); 

        return self.digest.result_str();
    }
}

fn add_file<T : Eq + Hash, SUM : Summarizer<T>>(fid : FID, sum : &mut SUM, store: &FileCollection, gf: &mut GroupedFiles<T>){
    let fd = store.get(fid);
    let key = sum.check_sum(fd);
    
    if let Some(files) = gf.get_mut(&key) {
        files.push(fid);
        return;
    } 
    // if it didn't exist add a new one 
    let _ = gf.insert(key, vec![fid]);
} 

fn all_in_one_group (store : &FileCollection) -> GroupedFiles<()> {
    let mut files : Vec<FID> = Vec::new();
    let mut gf = HashMap::new();
    if store.size() > 0 {
        for i in 0..(store.size() - 1) {
            files.push(i)
         }
    
        gf.insert((), files);
    }
    return gf;
} 

fn re_group<S : Eq + Hash, T : Eq + Hash, SUM : Summarizer<T>> (source: GroupedFiles<S>, store: &FileCollection, sum : &mut SUM) -> GroupedFiles<T> {
    let mut gf = HashMap::new();
    let mut saved = 0;
    for files in source.values() {
        if files.len() >= 2 { // only regroup non-singleton lists of files
            for file in files {
                add_file(file.clone(), sum, store, &mut gf)
            }
        } else {
            saved = saved + 1;
        }
    }
    println!("This phase saved {}", saved);
    return gf;
}

fn get_executable_name () -> String {
    let ex = match env::args().nth(0) {
        Some(x) => x,
        None => "twin".to_string()
    };
    return ex
}

fn get_path () -> Option<String> {
    return env::args().nth(1)
}



fn populate_file_collection<ST : FileCollection> (start_path : std::path::PathBuf, recursive: bool, store: &mut ST) {
    // try to read the file
    match fs::read_dir(start_path) {
        Err(_) => (), // this entry was not readable, move on to the next
        Ok(dir)  =>
            for entry in dir {
                let pth_buf = entry.expect("This is impossible!").path();
                match fs::metadata(pth_buf.clone()) {
                    Err(_) => (), // some file didn't have metadata
                    Ok(md) => if md.is_file() {
                        let _ = store.add(FileDesc{name: pth_buf, size: md.len()});
                    } else if md.is_dir() {
                        if recursive {
                            populate_file_collection(pth_buf, recursive, store);
                        }
                    }
                }
        }
    }
}

fn get_size_of_first<ST: FileCollection>(files: &Vec<FID>, store: &ST) -> u64{
    if files.len() == 0 { return 0}
    let fid = files[0];
    let fd = store.get(fid);
    return fd.size;
}

fn size_to_human (s : u64) -> String {
    let m = 1024 * 1024;
    let k = 1024;

    if s > m {
        return format!("{} MBs", s / m)
    } else if s > k {
        return format!("{} KBs", s / k)
    } else {
        return format!("{} bytes", s)
    }
}

fn print_groups<T : Eq + Hash, ST: FileCollection> (fgs : & GroupedFiles<T>, store: &ST) {
    let mut wasted : u64 = 0;
    for (_, group) in fgs {
        let num_files = group.len() as u64;
        if num_files > 1 {
            let size = get_size_of_first(group, store);
            println!("* {} files in the group of size: {}", num_files, size_to_human(size));
            wasted += size * (num_files - 1);
            for fid in group {
                let entry = store.get(fid.clone());
            
             println!("\tName: {}", entry.name.display());

            }
        }
    }
    println!("Total wasted space: {}", size_to_human(wasted));
}


fn start (start_path_str : &String, recursive: bool) {
    //let mut digest = crypto::md5::Md5::new();
    println!("Running on: {}", start_path_str);

    let start_path = path::Path::new(start_path_str).to_path_buf();
    let mut store = VecFileCollection::new();
    populate_file_collection(start_path, recursive, &mut store);

    let initial_group = all_in_one_group(&store);

    let mut summarizer : SizeSummary = SizeSummary{}; 
    let second_group = re_group(initial_group, &store, &mut summarizer);

    let md5_digest = crypto::md5::Md5::new();
    let mut md5_summarizer = MD5DigestSummary{digest : md5_digest };

    let third_group = re_group(second_group, &store, &mut md5_summarizer);

    println!("Scanned {} files", store.size());
    print_groups(&third_group, &store);
    println!("Resulting groups {}", third_group.len());
}

fn main() {

    let version_str = format!("{}.{}.{}{}",
                      env!("CARGO_PKG_VERSION_MAJOR"),
                      env!("CARGO_PKG_VERSION_MINOR"),
                      env!("CARGO_PKG_VERSION_PATCH"),
                      option_env!("CARGO_PKG_VERSION_PRE").unwrap_or(""));

    let matches = App::new("twins")
//                        .version(crate_version!())
//                        .author(crate_authors!())
                        .about("Duplicate file finder")
                        .arg(Arg::with_name("non_recursive")
                            .short("n")
                            .long("not_recursive")
                            .help("Scans only the specified directory not the recursive ones"))
                        .arg(Arg::with_name("path")
                            .value_name("PATH")
                            .takes_value(true)
                            //.required(true)
                            .help("Sets the starting path")).get_matches();

    let recursive = !matches.is_present("non_recursive");
    let path = matches.value_of("path").unwrap_or(".");
    
    println!("Starting with: {}", path);

    start(&path.to_string(), recursive);
    }
